<?php
/*
Plugin Name: Hide Bar & Dash
Version: 0.1
Description: Hides the admin bar for non admin logged in users and restricts access to /wp-admin dashboard.
Author: Elvis Morales
Author URI: https://twitter.com/n3rdh4ck3r
Plugin URI: https://bitbucket.org/grantcardone/hide-bar-dash
Text Domain: hide-bar-dash
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

// Remove admin bar for non administrators
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}

/**
* Redirect back to homepage and not allow access to
* WP admin for Subscribers.
*/
add_action( 'admin_init', 'redirect_no_admin' );
function redirect_no_admin() {
	if ( !current_user_can('administrator') ) {
		wp_redirect( site_url() );
		exit;
	}
}